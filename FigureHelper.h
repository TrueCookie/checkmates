//
// Created by max on 4/25/2021.
//

#ifndef CHECKMATES_FIGUREHELPER_H
#define CHECKMATES_FIGUREHELPER_H

enum Type {
    KING, PAWN, KNIGHT
};

enum Color {
    BLACK, WHITE
};

#endif //CHECKMATES_FIGUREHELPER_H

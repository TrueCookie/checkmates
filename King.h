//
// Created by max on 5/3/2021.
//

#ifndef CHECKMATES_KING_H
#define CHECKMATES_KING_H


#include "Figure.h"

class King : public Figure {

public:
    King(Color color, Type type, pair<int, int> pair);
    string getStatus(Figure figures[], int size);
};


#endif //CHECKMATES_KING_H

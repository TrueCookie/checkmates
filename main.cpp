#include <iostream>
#include "Figure.h"
#include "King.h"

int main() {
    King whiteKing(WHITE, KING, pair<int, int>(3, 3));

    Figure blackKnight(BLACK, KNIGHT, pair<int, int>(5, 4));

    Figure figures[] = {whiteKing, blackKnight};
    int size = 2;

    string res = whiteKing.getStatus(figures, size);
    cout << __FUNCTION__ << ':' << res << endl;

    system("Pause");
    return 0;
};

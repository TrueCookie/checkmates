//
// Created by max on 5/3/2021.
//

#include "King.h"

King::King(Color color, Type type, pair<int, int> pair) : Figure(color, type, pair) {

}

string King::getStatus(Figure figures[], int size) {
    int x = this->getPosition().first;
    int y = this->getPosition().second;

    int availableMoves = 8;
    pair<int, int> *square;

    set<pair<int, int>> moveVectors = {
            *(new pair<int, int>(-1, -1)),
            *(new pair<int, int>(-1, 0)),
            *(new pair<int, int>(-1, 1)),
            *(new pair<int, int>(0, -1)),
            *(new pair<int, int>(0, 1)),
            *(new pair<int, int>(1, -1)),
            *(new pair<int, int>(1, 0)),
            *(new pair<int, int>(1, 1)),
    };

    for(auto moveVector : moveVectors)    //check if squares around king
    {
        square = new pair<int, int>(x + moveVector.first, y + moveVector.second);
        for(int f = 0; f < size; f++) //is attacked (by some of figure)
        {
            if (figures[f].getPosition() == this->getPosition() || isOutOfBorder(*square)) continue;

            bool squareUnderAttack = figures[f].canAttack(*square);
            if (squareUnderAttack) {
                availableMoves--;
                break;
            }
        }
    }

    //check if king itself is attacked
    bool attacked = this->isAttacked(figures, size);

    if (availableMoves == 0 && attacked) {
        return "CHECKMATE";
    }
    else if(attacked) {
        return "CHECK";
    }
    else if(availableMoves == 0) {
        return "PATTE";
    }
    else {
        return "FREE";
    }
}


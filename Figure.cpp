#include "Figure.h"
#include <utility>
#include <algorithm>
#include <iostream>

using namespace std;

Figure::Figure(Color color, Type type, pair<int, int> position) : color(color), type(type){
    this->position = position;
}

Figure::~Figure() = default;

pair<int, int> Figure::getPosition() {
    return this->position;
}

int Figure::getColor() const {
    return this->color;
}

Type Figure::getType() const{
    return (Type) this->type;
}

bool Figure::canAttack(const pair<int, int> &targetPos) {
    if (this->getPosition() == targetPos) return false;

    switch (this->getType()) {
        case PAWN:
            return isPawnAttacks(targetPos);
        case KNIGHT:
            return isKnightAttacks(targetPos);
        case KING:
            return isKingAttacks(targetPos);
        default: throw std::invalid_argument("Figure must be one of this type: PAWN, KNIGHT, KING");
    }
}

bool Figure::isAttacked(Figure figures[], int size) {
    for(int i = 0; i < size; i++)
    {
        //cout << figures[i].getPosition().first << ':' << figures[i].getPosition().second << endl;
        //cout << this->getPosition().first << ':' << this->getPosition().second << endl;
        if (figures[i].getPosition() == this->getPosition() || figures[i].getColor() == this->getColor()) continue;
        if(figures[i].canAttack(this->getPosition()))
        {
            return true;
        }
    }
    return false;
}

bool Figure::isPawnAttacks(const pair<int, int> &targetPos) {
    int x = this->getPosition().first;
    int y = this->getPosition().second;

    return this->getColor() == WHITE
    ? (targetPos == pair<int, int>(x + 1, y + 1) || targetPos == pair<int, int>(x - 1, y + 1))
    : (targetPos == pair<int, int>(x + 1, y - 1) || targetPos == pair<int, int>(x - 1, y - 1));
}

bool Figure::isKnightAttacks(const pair<int, int> &targetPos) {
    int x = this->getPosition().first;
    int y = this->getPosition().second;

    return (targetPos == pair<int, int>(x + 1, y + 2) || targetPos == pair<int, int>(x + 2, y + 1) ||
            targetPos == pair<int, int>(x - 1, y - 2) || targetPos == pair<int, int>(x - 2, y - 1) ||
            targetPos == pair<int, int>(x + 1, y - 2) || targetPos == pair<int, int>(x + 2, y - 1) ||
            targetPos == pair<int, int>(x - 1, y + 2) || targetPos == pair<int, int>(x - 2, y + 1)
    );
}

bool Figure::isKingAttacks(const pair<int, int> &targetPos) {
    int x = this->getPosition().first;
    int y = this->getPosition().second;

    return (targetPos.first >= x - 1 && targetPos.first <= x + 1) && (targetPos.second >= y - 1 && targetPos.second <= y + 1);
}

bool Figure::isOutOfBorder(const pair<int, int> &square) {
    return (square.first < 0 || square.first > 7)
    || (square.second < 0 || square.second > 7);
}

#ifndef CHECKMATES_FIGURE_H
#define CHECKMATES_FIGURE_H

using namespace std;

#include <utility>
#include <set>
#include "FigureHelper.h"

class Figure {
private:
    pair<int, int> position;
    const int color;
    const int type;

public:
    Figure(Color color, Type type, pair<int, int> position);
    ~Figure();

    pair<int, int> getPosition();

    [[nodiscard]] int getColor() const;

    bool canAttack(const pair<int, int> &targetPos);

    bool isAttacked(Figure figures[], int size);

    [[nodiscard]] Type getType() const;

    bool isPawnAttacks(const pair<int, int> &targetPos);

    bool isKnightAttacks(const pair<int, int> &targetPos);

    bool isKingAttacks(const pair<int, int> &targetPos);

    static bool isOutOfBorder(const pair<int, int> &square);
};

#endif //CHECKMATES_FIGURE_H
